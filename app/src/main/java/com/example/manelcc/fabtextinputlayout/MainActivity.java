package com.example.manelcc.fabtextinputlayout;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private TextInputLayout ilNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initComponents();


        //1 Referenciar las id de los widgets


        //2 implementar sus listeners


    }

    private void initComponents() {
        //TextInputLayout Nombre
        ilNombre = (TextInputLayout) findViewById(R.id.mainActivity_ilNombre);
        EditText nombre = (EditText) findViewById(R.id.mainActivity_etxtNombre);

        //TextInputLayout Password
        final TextInputLayout ilPassword = (TextInputLayout) findViewById(R.id.mainActivity_ilPassword);
        final EditText password = (EditText) findViewById(R.id.mainActivity_etxtPassword);

        //Button Login
        Button login = (Button) findViewById(R.id.mainActivity_btnLogin);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String passwordCorrecto = "pepe123";
                String passwordRecibido = password.getText().toString();

                if (passwordRecibido.equals(passwordCorrecto)) {
                    Snackbar.make(v, "ENHORABUENA TE HAS LOGEADO", Snackbar.LENGTH_LONG).show();
                } else {
                    ilPassword.setErrorEnabled(true);
                    ilPassword.setError("Te has equivocado, vuelve a intentarlo");
                }
            }
        });


        //FAB
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
